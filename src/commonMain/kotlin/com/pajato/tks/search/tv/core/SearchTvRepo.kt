package com.pajato.tks.search.tv.core

import com.pajato.tks.common.core.SearchKey
import com.pajato.tks.pager.core.PageData
import com.pajato.tks.pager.core.PagedResultTv

public interface SearchTvRepo {
    public suspend fun getSearchPageTv(key: SearchKey): PageData<PagedResultTv>
    public suspend fun getSearchResults(key: SearchKey): List<PagedResultTv>
}
